package com.fico.realsoap.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

    @RestController
    @RequestMapping("/dummySoap")
    @Slf4j
    public class SOAPController {



        @RequestMapping(value = "/open/soap11/NumberConversion.wso", method = RequestMethod.POST,consumes = {MediaType.ALL_VALUE} , produces = {MediaType.APPLICATION_XML_VALUE})
        public String getActualServiceCall(@RequestBody String data){

            log.info("I dont care about Input ---- {}",data);
            String response = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                    "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
                    "  <soap:Body>\n" +
                    "    <m:NumberToWordsResponse xmlns:m=\"http://www.dataaccess.com/webservicesserver/\">\n" +
                    "      <m:NumberToWordsResult>one hundred and eleven - SOAP V1 ENDPOINT </m:NumberToWordsResult>\n" +
                    "    </m:NumberToWordsResponse>\n" +
                    "  </soap:Body>\n" +
                    "</soap:Envelope>";
            return response;

        }
        @RequestMapping(value = "/open/soap12/error/NumberConversion.wso", method = RequestMethod.POST,consumes = {MediaType.ALL_VALUE} , produces = {MediaType.APPLICATION_XML_VALUE})
        public ResponseEntity<String> getErrorResponseV2(@RequestBody String data){

            log.info("I dont care about Input ---- {}",data);
            String error = "<?xml version=\"1.0\"?>\n" +
                    "<env:Envelope xmlns:env=http://www.w3.org/2003/05/soap-envelope>\n" +
                    "   <env:Body>\n" +
                    "      <env:Fault>\n" +
                    "         <env:Code>\n" +
                    "            <env:Value>env:Sender</env:Value>\n" +
                    "            <env:Subcode>\n" +
                    "               <env:Value>rpc:BadArguments</env:Value>\n" +
                    "            </env:Subcode>\n" +
                    "         </env:Code>\n" +
                    "         <env:Reason>\n" +
                    "            <env:Text xml:lang=en-US>Processing error<env:Text>\n" +
                    "         </env:Reason>\n" +
                    "         <env:Detail>\n" +
                    "            <e:myFaultDetails \n" +
                    "               xmlns:e=http://travelcompany.example.org/faults>\n" +
                    "               <e:message>Name does not match card number</e:message>\n" +
                    "               <e:errorcode>999</e:errorcode>\n" +
                    "            </e:myFaultDetails>\n" +
                    "         </env:Detail>\n" +
                    "      </env:Fault>\n" +
                    "   </env:Body>\n" +
                    "</env:Envelope>";
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(error);

        }


        @RequestMapping(value = "/open/soap11/error/NumberConversion.wso", method = RequestMethod.POST,consumes = {MediaType.ALL_VALUE} , produces = {MediaType.APPLICATION_XML_VALUE} )
        public ResponseEntity<String> getErrorResponseV1(@RequestBody String data){

            log.info("I dont care about Input ---- {}",data);
            String error = "<?xml version=\"1.0\"?>\n" +
                    "<soap:Envelope \n" +
                    "    xmlns:soap='http://schemas.xmlsoap.org/soap/envelope'>\n" +
                    "   <soap:Body>\n" +
                    "      <soap:Fault>\n" +
                    "         <faultcode>soap:VersionMismatch</faultcode>\n" +
                    "         <faultstring, xml:lang='en\">\n" +
                    "            Message was not SOAP 1.1 compliant\n" +
                    "         </faultstring>\n" +
                    "         <faultactor>\n" +
                    "            http://sample.org.ocm/jws/authnticator\n" +
                    "         </faultactor>\n" +
                    "      </soap:Fault>\n" +
                    "   </soap:Body>\n" +
                    "</soap:Envelope>";
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(error);

        }
        @RequestMapping(value = "/open/soap12/NumberConversion.wso", method = RequestMethod.POST, consumes = {MediaType.ALL_VALUE} , produces = {MediaType.APPLICATION_XML_VALUE} )
        public String getActualServiceCallV2(@RequestBody String data){

            log.info("I dont care about Input ---- {}",data);
            String response = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                    "<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\">\n" +
                    "  <soap:Body>\n" +
                    "    <m:NumberToWordsResponse xmlns:m=\"http://www.dataaccess.com/webservicesserver/\">\n" +
                    "      <m:NumberToWordsResult>one hundred and eleven - SOAP V2 ENDPOINT</m:NumberToWordsResult>\n" +
                    "    </m:NumberToWordsResponse>\n" +
                    "  </soap:Body>\n" +
                    "</soap:Envelope>";
            return response;

        }


    }
