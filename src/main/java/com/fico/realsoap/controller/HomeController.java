package com.fico.realsoap.controller;

import com.fico.realsoap.pojos.ResponseData;
import com.fico.realsoap.web.ServiceDemo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Date;

@RestController
@Slf4j
public class HomeController {

    @Autowired
    private ResourceLoader resourceLoader;

    @GetMapping("/pdf/sample/inline")
    public ResponseEntity<byte[]> getPdf() throws IOException {
        Resource resource = resourceLoader.getResource("classpath:SAMPLE.pdf");
        byte[] contents = resource.getContentAsByteArray();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_PDF);
        // Here you have to set the actual filename of your pdf
        String filename = "output.pdf";
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
        headers.add("Content-Disposition", "inline; filename=" +filename);
        ResponseEntity<byte[]> response = new ResponseEntity<>(contents, headers, HttpStatus.OK);
        return response;
    }

    @GetMapping("/pdf/sample/attachment")
    public ResponseEntity<byte[]> getPdfattachment() throws IOException {
        Resource resource = resourceLoader.getResource("classpath:SAMPLE.pdf");
        byte[] contents = resource.getContentAsByteArray();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_PDF);
        // Here you have to set the actual filename of your pdf
        String filename = "output.pdf";
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
        headers.add("Content-Disposition", "attachment; filename=" +filename);
        ResponseEntity<byte[]> response = new ResponseEntity<>(contents, headers, HttpStatus.OK);
        return response;
    }
    @GetMapping("/hello/{Code}")
    public ResponseEntity getData(@PathVariable  Integer Code){
        ResponseEntity<ResponseData> response;
        switch (Code){
            case 200:
                response= ResponseEntity.ok(ResponseData.builder().message("All is good !").date(new Date()).build() );
                break;
            case 400:
                response= ResponseEntity.badRequest().body(ResponseData.builder().message("I am bad request").date(new Date()).build());
                break;
            case 500:
                response= ResponseEntity.internalServerError().body(ResponseData.builder().message("I am internal server error").date(new Date()).build());
                break;
            default:
                throw new RuntimeException("I am not known to you");

        }
        return response;
    }

    @ExceptionHandler(value = RuntimeException.class)
    public ResponseEntity<Object> exception(RuntimeException exception) {
        return new ResponseEntity<>(ResponseData.builder().message(exception.getMessage()).date(new Date()).build(), HttpStatus.CONFLICT);
    }


    @PostMapping(value = "/hello/soap/rma" , consumes = {MediaType.ALL_VALUE} , produces = {MediaType.APPLICATION_XML_VALUE})
    public String dummySOAPResponseForPSIssueRMS(@RequestBody String data) throws IOException {
        log.error("Request Received from User  - \n");
        Resource resource = resourceLoader.getResource("classpath:dummySopaRMA.xsd");
        return resource.getContentAsString(StandardCharsets.UTF_8);
    }


    @PostMapping(value = "/hello/soap/wfs" , consumes = {MediaType.ALL_VALUE} , produces = {MediaType.APPLICATION_XML_VALUE})
    public String dummySOAPResponseForPSIssueWFS(@RequestBody String data) throws IOException {
        log.error("Request Received from User  - \n");
        Resource resource = resourceLoader.getResource("classpath:dummySopaWFS.xsd");
        return resource.getContentAsString(StandardCharsets.UTF_8);
    }


    @GetMapping(value = "/api/wsdl/v2/NUMBER" ,  produces = {MediaType.APPLICATION_XML_VALUE})
    public String dummySOAPForNumber() throws IOException {
        log.error("Request Received from User  - \n");
        Resource resource = resourceLoader.getResource("classpath:NumberToWordl.xsd");
        return resource.getContentAsString(StandardCharsets.UTF_8);
    }

}
