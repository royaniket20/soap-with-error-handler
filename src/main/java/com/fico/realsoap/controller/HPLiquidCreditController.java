package com.fico.realsoap.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

@RestController
@RequestMapping("/liquidcredit")
@Slf4j
public class HPLiquidCreditController {

    @Autowired
    private ResourceLoader resourceLoader;


    @RequestMapping(value = "/hp/data", method = RequestMethod.POST,consumes = {MediaType.ALL_VALUE} , produces = {MediaType.APPLICATION_XML_VALUE})
    public String sendResponseStatic(@RequestBody String data) throws IOException {
        log.error(" Received payload --- \n {}", data);
        Resource resource = resourceLoader.getResource("classpath:Resposne-liquidcredit-success.xml");
        return resource.getContentAsString(StandardCharsets.UTF_8);
    }
}
