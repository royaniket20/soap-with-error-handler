package com.fico.realsoap.repository;

import com.fico.realsoap.models.Country;
import com.fico.realsoap.models.Currency;
import jakarta.annotation.PostConstruct;
import jakarta.xml.soap.SOAPFault;
import jakarta.xml.ws.soap.SOAPFaultException;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;

@Component
public class CountryRepository {
    private static final Map<String, Country> countries = new HashMap<>();

    @PostConstruct
    public void initData() {
        Country spain = new Country();
        spain.setName("Spain");
        spain.setCapital("Madrid");
        spain.setCurrency(Currency.EUR);
        spain.setPopulation(46704314);

        countries.put(spain.getName(), spain);

        Country poland = new Country();
        poland.setName("Poland");
        poland.setCapital("Warsaw");
        poland.setCurrency(Currency.PLN);
        poland.setPopulation(38186860);

        countries.put(poland.getName(), poland);

        Country uk = new Country();
        uk.setName("United Kingdom");
        uk.setCapital("London");
        uk.setCurrency(Currency.GBP);
        uk.setPopulation(63705000);

        countries.put(uk.getName(), uk);
    }

    public Country findCountry(String name) {
        if(StringUtils.isEmpty(name)){
            //Assert.notNull(name, "The country's name must not be null");
            String errorMsg = null;
          throw new RuntimeException(errorMsg);
        }
        if(!countries.containsKey(name))
            throw new RuntimeException("Name must be from allowed list of countries - Provided is " + name);
        return countries.get(name);
    }
}
