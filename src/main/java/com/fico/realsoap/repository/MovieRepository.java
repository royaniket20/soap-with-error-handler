package com.fico.realsoap.repository;

import com.fico.realsoap.exception.ServiceFaultException;
import com.fico.realsoap.models.MovieType;
import com.fico.realsoap.models.ServiceStatus;
import jakarta.annotation.PostConstruct;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;

@Component
public class MovieRepository {
    private static List<MovieType> movies = new ArrayList<>();
    @PostConstruct
    public void initData() {
        MovieType movie1 = new MovieType();
        movie1.setMovieId(101);
        movie1.setTitle("Mission Impossible");
        movie1.setCategory("Action");

        movies.add(movie1);

        MovieType movie2 = new MovieType();
        movie2.setMovieId(102);
        movie2.setTitle("Argo");
        movie2.setCategory("Drama");
        movies.add(movie2);

        MovieType movie3 = new MovieType();
        movie3.setMovieId(103);
        movie3.setTitle("Hang over");
        movie3.setCategory("Comedy");
        movies.add(movie3);

    }

    // Get All movies
    public List<MovieType> findAll() {
        return movies;
    }

    // Get Movie By Id - 1
    public MovieType findById(long id) {
        MovieType result = null;
        for (MovieType movie : movies) {
            if (movie.getMovieId() == id)
                result =  movie;
        }
       // Assert.notNull(result , "No Movie found for the Given Id - "+id);
        if(result!=null) {
            return result;
        }else{
            String errorMessage = "INTERNAL CUSTOM ERROR";
            ServiceStatus serviceStatus = new ServiceStatus();
            serviceStatus.setStatusCode("NOT_FOUND");
            serviceStatus.setMessage("Movie with id: " + id + " not found. Give some valid Id");

            throw new ServiceFaultException(errorMessage, serviceStatus);
        }
    }

}
