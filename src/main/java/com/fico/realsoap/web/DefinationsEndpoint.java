package com.fico.realsoap.web;

import com.fico.realsoap.models.GetAllMoviesRequest;
import com.fico.realsoap.models.GetAllMoviesResponse;
import com.fico.realsoap.models.GetCountryRequest;
import com.fico.realsoap.models.GetCountryResponse;
import com.fico.realsoap.models.GetMovieByIdRequest;
import com.fico.realsoap.models.GetMovieByIdResponse;
import com.fico.realsoap.repository.CountryRepository;
import com.fico.realsoap.repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class DefinationsEndpoint {
    private static final String NAMESPACE_URI = "http://fico.com/RealSoap/models";

    private CountryRepository countryRepository;

    private MovieRepository movieRepository;

    @Autowired
    public DefinationsEndpoint(CountryRepository countryRepository, MovieRepository movieRepository) {
        this.countryRepository = countryRepository;
        this.movieRepository=movieRepository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getCountryRequest")
    @ResponsePayload
    public GetCountryResponse getCountry(@RequestPayload GetCountryRequest request) {
        GetCountryResponse response = new GetCountryResponse();
        response.setCountry(countryRepository.findCountry(request.getName()));

        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getMovieByIdRequest")
    @ResponsePayload
    public GetMovieByIdResponse getMovie(@RequestPayload GetMovieByIdRequest request) {
        GetMovieByIdResponse response = new GetMovieByIdResponse();
        response.setMovieType(movieRepository.findById(request.getMovieId()));
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllMoviesRequest")
    @ResponsePayload
    public GetAllMoviesResponse getMovies(@RequestPayload GetAllMoviesRequest request) {
        GetAllMoviesResponse response = new GetAllMoviesResponse();
        response.getMovieType().addAll(movieRepository.findAll());
        return response;
    }
}
