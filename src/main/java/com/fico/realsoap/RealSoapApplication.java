package com.fico.realsoap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RealSoapApplication {

	public static void main(String[] args) {
		SpringApplication.run(RealSoapApplication.class, args);
	}

}
